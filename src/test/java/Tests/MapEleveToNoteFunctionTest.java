package Tests;


import fr.iiil.fp.romaric.entities.Eleve;
import fr.iiil.fp.romaric.functions.MapEleveToNoteFunction;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.junit.Assert.*;

    @Slf4j
    public class MapEleveToNoteFunctionTest {

        @Test
        public void testApply() {
            Eleve eleve = new Eleve("Cabrel" , 15.0) ;
            MapEleveToNoteFunction maFonctionNote = new MapEleveToNoteFunction();
            assertEquals( maFonctionNote.apply(eleve)  , Double.valueOf(15.0) );

            log.info("MapNoteTest  test {}" , maFonctionNote.apply(eleve) );
        }
    }
