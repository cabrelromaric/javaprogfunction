package Tests;


import fr.iiil.fp.romaric.entities.Eleve;
import fr.iiil.fp.romaric.functions.EleveSupplier;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.junit.Assert.*;

@Slf4j
public class EleveSupplierTest {


    @Test
    public void testGet() {
        Eleve eleve = new Eleve("Cabrel" , 15.0) ;
        EleveSupplier monSupplier = new EleveSupplier();
        assertEquals(monSupplier.get().getNom(), "Romaric");
        log.info("supplier test {}" , monSupplier.get() );
    }
}