package fr.iiil.fp.romaric.test;



import fr.iiil.fp.romaric.entities.Eleve;
import fr.iiil.fp.romaric.functions.EleveLogger;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class EleveLoggerTest  {

    @Test
    public void testAccept() {

        EleveLogger monLogger = new EleveLogger() ;
        Eleve monEleve = new Eleve("jean"  , 12.0) ;

        monLogger.accept(monEleve);


    }
}