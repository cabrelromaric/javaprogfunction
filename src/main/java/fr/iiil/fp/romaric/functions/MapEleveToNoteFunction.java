package fr.iiil.fp.romaric.functions;

import fr.iiil.fp.romaric.entities.Eleve;


import java.util.function.Function;

public class MapEleveToNoteFunction implements Function<Eleve , Double> {

    @Override
    public Double apply( Eleve eleve) {
        return eleve.getNote() ;
    }
}
