package fr.iiil.fp.romaric.functions;

import fr.iiil.fp.romaric.entities.Eleve;

import java.util.Random;
import java.util.function.Supplier;

public class EleveSupplier implements Supplier<Eleve> {
    final Random random  =new Random() ;

    @Override
    public Eleve get() {
        return new Eleve("Romaric" , random.nextDouble()) ;
    }
}
