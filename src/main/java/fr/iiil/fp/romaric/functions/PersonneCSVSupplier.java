package fr.iiil.fp.romaric.functions;

import fr.iiil.fp.romaric.entities.Personne;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PersonneCSVSupplier implements Supplier<Stream<String>> {
    @Override
    public Stream<String> get() {
        Path source = Paths.get("src/main/resources/personnes.csv");
        if(source != null ) {
            Stream<String> lines = null;
            try {
                lines = java.nio.file.Files.lines(source).skip(1);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return lines ;
    } else {
            return  Stream.of("");
        }
    }
}
