package fr.iiil.fp.romaric.functions;

import fr.iiil.fp.romaric.entities.Personne;

import java.util.function.Function;

public class MapLigneVersPersonne implements Function<String , Personne> {

    @Override
    public Personne apply(String s) {
        String[] parties = s.split(",");
        String prenom = parties[0].trim();
        int age = Integer.valueOf(parties[1].trim());
        return new Personne(prenom, age);
    }
}
