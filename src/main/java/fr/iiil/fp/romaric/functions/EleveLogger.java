package fr.iiil.fp.romaric.functions;

import fr.iiil.fp.romaric.entities.Eleve;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

@Slf4j
public class EleveLogger implements Consumer<Eleve> {

    public void accept(Eleve eleve) {
        log.info("Eleve : {}" , eleve) ;
    }
}
