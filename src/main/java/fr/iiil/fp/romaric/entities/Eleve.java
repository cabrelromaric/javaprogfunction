package fr.iiil.fp.romaric.entities;

import lombok.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter(AccessLevel.PUBLIC)
public class Eleve {

    private String nom ;
 //   @Getter() @Setter
    private Double note ;


}
