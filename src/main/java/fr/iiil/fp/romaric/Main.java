package fr.iiil.fp.romaric;
import fr.iiil.fp.romaric.entities.Eleve;
import fr.iiil.fp.romaric.entities.Personne;
import fr.iiil.fp.romaric.functions.MapLigneVersPersonne;
import fr.iiil.fp.romaric.functions.PersonneCSVSupplier;
import lombok.extern.slf4j.Slf4j ;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException {
            // lire le fichier en stream
        try{



                log.info("=============");

            PersonneCSVSupplier personneCsvSupplier = new PersonneCSVSupplier() ;

                log.info("=============");

                // creation de la liste des personnes 8.1
                MapLigneVersPersonne mapLigneVersPersonne = new MapLigneVersPersonne() ;
                List<Personne> listePersonnes = personneCsvSupplier.get().map(
                                line -> mapLigneVersPersonne.apply(line)
                        ).filter(Objects::nonNull)
                        .toList();

                log.info("=============");
                log.info("Affichage de la liste des personnes ");
                listePersonnes.forEach( personne -> log.info("{}" ,personne) );




                // exercice 8.2 affichage de la liste par ordre age croissant

                // Tri de la liste par âge croissant
                List<Personne> personnesAgeCroissant = listePersonnes.stream()
                        .sorted((p1, p2) -> Integer.compare(p1.getAge(), p2.getAge()))
                        .collect(Collectors.toList());

                // Affichage de la liste triée
                log.info("=============");
                log.info("Affichage de la liste des personnes par age croissant");
                personnesAgeCroissant.forEach( personne -> log.info("{}" , personne ) );



                // exercice 8.3 affichage de la liste par ordre croissant
                Comparator<Personne> comparateurParAgeCroissant = Comparator.comparing(Personne::getAge);

                List<Personne> personnesTrieComparateur = listePersonnes.stream()
                        .sorted(comparateurParAgeCroissant)
                        .collect(Collectors.toList());

                // Affichage de la liste  des personnes par age croissant avec Comparator
                log.info("=============");
                log.info("Affichage de la liste des personnes par age decroissant ");
                personnesTrieComparateur.forEach(personne -> log.info("{}" , personne ));
                log.info("=============");


                    // exercice 8.4
                // Definition du comparateur
                Comparator<Personne> comparateurAgePrenom = Comparator
                        .comparing(Personne::getAge)
                        .reversed()
                        .thenComparing(Personne::getPrenom);

                // Definition du comparateur par âge décroissant puis par prénom croissant
                Comparator<Personne> comparateurPrenom = Comparator
                        .comparing(Personne::getPrenom);
                // Tri de la liste par âge décroissant puis par prénom croissant
                List<Personne> personnesTrieesAgeCPrenomD = listePersonnes.stream()
                        .sorted(comparateurParAgeCroissant.reversed())
                        .sorted(comparateurPrenom)
                        .collect(Collectors.toList());

                // Affichage de la liste triée
                log.info("=============");
                log.info("Affichage de la liste des personnes par age decroissant puis par prenom croissant");
                personnesTrieesAgeCPrenomD.forEach(personne -> log.info("{}" , personne ));


        }catch ( Exception e) {
            log.info("=============");
            log.info("une erreru s est produite");
            e.getMessage();
            e.printStackTrace();
            log.info("=============");

        }



    }
}